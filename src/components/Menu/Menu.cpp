#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include "../../core/ui.h"
#include "Menu.h"

namespace ui {
	// ============ Menu Component ============
	Menu::Menu(): Component() {
		this->_menu_keys = {};
		this->_menu_handlers = {};
	}
	
	void Menu::add(std::string key, std::function<bool()> handler) {
		this->_menu_keys.push_back(key);
		this->_menu_handlers.push_back(handler);
	}
	
	void Menu::clear() {
		this->_menu_keys.clear();
		this->_menu_handlers.clear();
	}
	
	bool Menu::display() {
		int menu_size = this->_menu_keys.size();
		
		for (int i = 0; i < menu_size; i++) {
			printf("%d. %s\n", i + 1, this->_menu_keys[i].c_str());
		}
		
		printf("\nChoose menu: ");
    std::string input;
    std::getline(std::cin, input);
		
		int choosed_menu = std::stoi(input);
		
		if (choosed_menu > 0 && choosed_menu <= menu_size) {
			bool handler_result = this->_menu_handlers[choosed_menu - 1]();
			
			return handler_result;
		}
		
		return false;
	}
}
