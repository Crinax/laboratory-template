#pragma once
#include <string>
#include <functional>
#include <vector>
#include "../../core/ui.h"

namespace ui {
	class Menu : public Component {
		public:
			Menu();
			
			void add(std::string, std::function<bool()>);
			void clear();
			bool display();

		private:
			std::vector<std::string> _menu_keys;
			std::vector<std::function<bool()>> _menu_handlers;		
	};
}