#pragma once
#include <functional>
#include <string>
#include "../../core/ui.h"
#include "../InputComponent/InputComponent.h"

namespace ui {
	template<class T>
	class InputReaderComponent : public Component {
		public:
      InputReaderComponent(T& input, std::function<T(std::string)> parser):
        _input(input),
        _parser(parser)
      {}

			bool display() {
        std::function<T(std::string)> parser = this->_parser;
        InputComponent input_field = {
          [this, parser](std::string result) mutable -> bool {
            T value = parser(result);
            this->_input = value;

            return false;
          }
        };
        
        return input_field.display();
      }
		
		private:
			T& _input;
			std::function<T(std::string)> _parser;
	};
}
