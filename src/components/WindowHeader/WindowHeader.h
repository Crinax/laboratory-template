#pragma once
#include <string>
#include "../../core/ui.h"

namespace ui {
	class WindowHeader : public Component {
		public:
			WindowHeader(std::string);
			
			bool display();
		
		private:
			std::string _text;
	};
}