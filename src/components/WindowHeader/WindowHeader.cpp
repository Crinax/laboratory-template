#include <string>
#include "../../core/ui.h"
#include "WindowHeader.h"

namespace ui {
	// ============ Window Header Component ============
	WindowHeader::WindowHeader(std::string header):
		Component()
	{
		this->_text = header;
	}
	
	bool WindowHeader::display() {
		printf("=== %s ===\n\n", this->_text.c_str());
		
		return false;
	}
}
