#include <vector>
#include "../../core/ui.h"
#include "../../core/console.h"
#include "ErrorDiffuser.h"

namespace ui {
	// ============ Error Diffuser ============
	ErrorDiffuser::ErrorDiffuser(
		std::vector<Component*> components
	): Combinator(components) { }
	
	bool ErrorDiffuser::display() {
		bool is_repeat = true;
		bool need_to_rerender = false;
		int components_count = this->_components.size();
		
		while (is_repeat) {
			int i = 0;
			for (; (i < components_count) && !need_to_rerender; i++) {
				try {
					this->_components[i]->display();
				} catch(...) {
					console::clear_lines_before(i);
					need_to_rerender = true;
				}
			}
			
			if (need_to_rerender) {
				need_to_rerender = false;
				continue;
			}
			
			is_repeat = false;
		}
		
		return false;
	}
}