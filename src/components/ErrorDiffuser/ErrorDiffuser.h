#pragma once
#include <vector>
#include "../../core/ui.h"

namespace ui {
	class ErrorDiffuser : public Combinator {
		public:
			ErrorDiffuser(std::vector<Component*>);
			
			bool display();
	};
}