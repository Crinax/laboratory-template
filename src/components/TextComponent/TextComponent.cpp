#include <string>
#include "../../core/ui.h"
#include "TextComponent.h"

namespace ui {
	// ============ Text Component ============
	TextComponent::TextComponent(std::string text):
		Component(),
		_text(text)
	{ }
	
	void TextComponent::replace(std::string text) {
		this->_text = text;
	}
	
	bool TextComponent::display() {
		printf("%s", this->_text.c_str());
		
		return false;
	}
}
