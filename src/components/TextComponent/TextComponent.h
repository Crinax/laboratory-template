#pragma once
#include <string>
#include "../../core/ui.h"

namespace ui {
	class TextComponent : public Component {
		public:
			TextComponent(std::string);
			
			bool display();
			void replace(std::string);

		private:
			std::string _text;
	};
}