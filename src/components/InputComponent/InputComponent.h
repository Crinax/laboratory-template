#pragma once
#include <string>
#include <functional>
#include "../../core/ui.h"

namespace ui {
	class InputComponent : public Component {
		public:
			InputComponent(std::function<bool(std::string)>);
			
			bool display();
			
		private:
			std::string _text;
			std::function<bool(std::string)> _handler;
	};
}