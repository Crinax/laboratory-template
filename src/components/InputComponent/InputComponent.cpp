#include <iostream>
#include <string>
#include <functional>
#include <cstdio>
#include "../../core/ui.h"
#include "InputComponent.h"

namespace ui {
	// ============ Input Component ============
	InputComponent::InputComponent(std::function<bool(std::string)> handler):
		Component(),
		_handler(handler)
	{ }
	
	bool InputComponent::display() {
    std::string result;
    std::getline(std::cin, result);

		return this->_handler(result);
	}
}
