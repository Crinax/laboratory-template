#include <cstdio>
#include <string>
#include <vector>
#include <algorithm>
#include "ui.h"

namespace ui {
	// ============ Component ============
	Component::Component() {}
	
	bool Component::display() {
		return false;
	}
	
	// ============ Window Manager Component ============
	WindowManager::WindowManager(): Component() {
		this->_draw_functions = { };
		this->_keys = { };
		this->_current_window = 0;
	}
	
	void WindowManager::add(
		std::string key,
		std::function<bool(WindowManager*)> draw_function
	)
	{
		this->_draw_functions.push_back(draw_function);
		this->_keys.push_back(key);
	
		if (this->_current_window == 0) {
			this->_current_window++;
		}
	}
	
	unsigned int WindowManager::count() {
		return this->_draw_functions.size();
	}
	
	bool WindowManager::switch_to(std::string window) {
		auto iterator = std::find(
			this->_keys.begin(),
			this->_keys.end(),
			window
		);
		
		if (iterator == this->_keys.end()) {
			return false;
		}
		
		this->_current_window = iterator - this->_keys.begin() + 1;
		return true;
	}
	
	unsigned int WindowManager::get_current_window() {
		return this->_current_window;
	}
	
	bool WindowManager::display() {
		if (this->_current_window == 0) {
			printf("Error: Windows was not added");
			
			return true;
		}
		
		return this->_draw_functions[this->_current_window - 1](this);
	}
	
	// ============ Combinator ============
	Combinator::Combinator(
		std::vector<Component*> components
	): _components(components) { };
	
	void Combinator::replace(
		std::vector<Component*> components
	)
	{
		this->_components = components;
	}
	
	void Combinator::add(
		Component* component
	)
	{
		this->_components.push_back(component);
	}
	
	// ============ Error Message Component ============
	ErrorMessage::ErrorMessage(std::string text):
		Component()
	{
		this->_text = text;
	}
	
	bool ErrorMessage::display() {
		printf("\n===> Error: %s\n", this->_text.c_str());
		
    getchar();
		
		return false;
	}
}
