#pragma once
#include <string>
#include <vector>
#include <functional>
#include "console.h"

namespace ui {
	class Component {
		public:
			Component();
			
			virtual bool display();
	};
	
	class WindowManager : public Component {
		public:
			WindowManager();
			
			void add(std::string, std::function<bool(WindowManager*)>);
			bool display();
			unsigned int count();
			bool switch_to(std::string window);
			unsigned int get_current_window();

		private:
			std::vector<std::function<bool(WindowManager*)>> _draw_functions;
			std::vector<std::string> _keys;
			unsigned int _current_window;
	};
	
	class Combinator : public Component {
		public:
			Combinator(std::vector<Component*>);
			
			virtual bool display() {
				int components_count = this->_components.size();
			
				for (int i = 0; i < components_count; i++) {
					this->_components[i]->display();
				}
				
				return false;
			};
			
			void replace(std::vector<Component*>);
			void add(Component*);
			
		protected:
			std::vector<Component*> _components;
	};
	
	class ErrorMessage : public Component {
		public:
			ErrorMessage(std::string);
			
			bool display();
		
		private:
			std::string _text;
	};
}