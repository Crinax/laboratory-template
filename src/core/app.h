#pragma once
#include <memory>
#include <functional>
#include "console.h"
#include "ui.h"

namespace application {
	class Module {
		public:
			Module();
			
			void load(ui::WindowManager*);
			virtual void load_dependencies(ui::WindowManager*);
		
		protected:
	};
	
	class App {
		public:
			App();
			
			int run();
			void initialize_loop(console::LoopManager*);
			void load_module(Module*);
			bool switch_window(std::string);
			
		private:
			console::Manager _console_manager;
			ui::WindowManager _window_manager;
	};
}