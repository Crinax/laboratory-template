#include <memory>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "console.h"
#include "app.h"
#include "ui.h"

// ============ App ============
application::App::App() {
	this->_console_manager = {};
	this->_window_manager = {};
}

bool application::App::switch_window(
	std::string menu
)
{
	bool is_switched = this->_window_manager.switch_to(menu);
	
	if (!is_switched) {
		ui::ErrorMessage error = {
			"Window \"" + menu + "\" not exists"
		};
		
		error.display();
	}
	
	return false;
}

void application::App::initialize_loop(
	console::LoopManager *loop_manager
)
{
	bool display_result = this->_window_manager.display();
	if (display_result) {
		loop_manager->stop();
	}
}

void application::App::load_module(application::Module* module) {
	module->load(&this->_window_manager);
}

int application::App::run() {
  auto loop_func = std::bind(
    &App::initialize_loop,
    this,
    std::placeholders::_1
  );
  
  this->_console_manager.run_loop(loop_func);
	
	return 0;
}

// ============ App Module ============
application::Module::Module() {};

void application::Module::load(
	ui::WindowManager* window_manager
)
{
	this->load_dependencies(window_manager);
}

void application::Module::load_dependencies(ui::WindowManager*) { }
